import './style.css';
import * as three from 'three';
import * as dat from 'dat.gui';

const loader = new three.TextureLoader();
const height = loader.load('./height.png');
const texture = loader.load('./texture.jpg');
const alpha = loader.load('./alpha.png');

// Canvas
const canvas = document.querySelector('canvas.webgl');

// Scene
const scene = new three.Scene();

// Objects
const geometry = new three.PlaneBufferGeometry(3, 3, 64, 64);

// Materials
const material = new three.MeshStandardMaterial({
    color: 'gray',
    map: texture,
    displacementMap: height,
    displacementScale: 1,
    alphaMap: alpha,
    transparent: true,
    depthTest: false,
});

const plane = new three.Mesh(geometry, material);
plane.rotation.x = 181;
scene.add(plane);
// Mesh

// Lights

const pointLight = new three.PointLight('#00b3ff', 3);
pointLight.position.x = 0.2;
pointLight.position.y = 10;
pointLight.position.z = 4.4;
scene.add(pointLight);

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight,
};

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth;
    sizes.height = window.innerHeight;

    // Update camera
    camera.aspect = sizes.width / sizes.height;
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

/**
 * Camera
 */
// Base camera
const camera = new three.PerspectiveCamera(
    75,
    sizes.width / sizes.height,
    0.1,
    100
);
camera.position.x = 0;
camera.position.y = 0.5;
camera.position.z = 3;
scene.add(camera);

// Controls
// const controls = new OrbitControls(camera, canvas)
// controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new three.WebGLRenderer({
    canvas: canvas,
    alpha: true,
});
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * Animate
 */

const clock = new three.Clock();

const tick = () => {
    const elapsedTime = clock.getElapsedTime();

    // Update objects
    plane.rotation.z = 0.5 * elapsedTime;

    // Update Orbital Controls
    // controls.update()

    // Render
    renderer.render(scene, camera);

    // Call tick again on the next frame
    window.requestAnimationFrame(tick);
};

tick();
